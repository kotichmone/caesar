﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Xceed.Words.NET;
using System.Collections;

namespace SimanovichCaesarKurs.Models
{
    public class shifr
    {

        public ArrayList Shifr(int vectorShirf, int key, ArrayList listOfValue) //Метод шифрования
        {
            ArrayList will = new ArrayList();
            try
            {

                if (key == 0) //в случае, если ключ =0, то выводится введенный массив
                {
                    will = listOfValue;
                }
                else
                {

                    foreach (string s in listOfValue)
                    {
                        if (s != null)
                        {
                            string afterString = "";
                            for (int i = 0; i < s.Length; i++)
                            {
                                if (key >= 33) //если ключ больше количества русского алфавита
                                {
                                    key = -31;
                                }
                                char simbol = s[i];
                                int everySimbol = Convert.ToInt32(simbol);

                                if ((everySimbol > 1039) || (everySimbol == 1025)) //если русский алфавит и ё 
                                {


                                    if (vectorShirf == 0) //вектор направления вправо
                                    {
                                        if ((everySimbol <= 1071) && (everySimbol + key > 1071) || (everySimbol <= 1103) && (everySimbol + key > 1103)) //если при шифровании уходит за границы алфавита
                                        {

                                            everySimbol += key;
                                            everySimbol -= 32;
                                        }
                                        else if ((everySimbol <= 1103) && (everySimbol + key > 1103) || (everySimbol <= 1071) && (everySimbol + key > 1071))
                                        {
                                            if ((everySimbol >= 1072) && (everySimbol <= 1077) && (everySimbol + key > 1077))
                                            { everySimbol++; }
                                            else if ((everySimbol >= 1040) && (everySimbol <= 1045) && (everySimbol + key >= 1045))
                                            { everySimbol++; }

                                            else if (everySimbol == 1025) // если символ Ё
                                            {
                                                everySimbol = 1046;
                                                everySimbol -= key;

                                            }
                                            else if (everySimbol == 1105)//если символ ё
                                            {

                                                everySimbol = 1078;
                                                everySimbol -= key;
                                            }
                                            else
                                            {
                                                everySimbol += key;
                                                everySimbol -= 32;
                                            }
                                        }

                                        else { everySimbol += key; }
                                    }
                                    else if (vectorShirf == 1)
                                    {
                                        {
                                            // Ё 1025 ё 1105 е 1077 Е 1045
                                            //я 1103 Я 1171 а 1072 А 1040

                                            if ((everySimbol >= 1072) && (everySimbol - key < 1072) || (everySimbol >= 1040) && (everySimbol - key < 1040)) //если алфавит вылезает за рамки
                                            {

                                                everySimbol -= key;
                                                everySimbol += 32;
                                            }
                                            else if ((everySimbol >= 1077) && (everySimbol <= 1103) && (everySimbol - key == 1076)) //если символ был от е до я, а станет е
                                            {

                                                everySimbol++;
                                                everySimbol -= key;

                                            }
                                            else if ((everySimbol >= 1045) && (everySimbol <= 1071) && (everySimbol - key == 1044)) //если символ был от Е до Я, а станет Е
                                            {

                                                everySimbol++;
                                                everySimbol -= key;

                                            }
                                            else if (everySimbol == 1025) // если символ Ё
                                            {
                                                everySimbol = 1046;
                                                everySimbol -= key;

                                            }
                                            else if (everySimbol == 1105)//если символ ё
                                            {

                                                everySimbol = 1078;
                                                everySimbol -= key;
                                            }

                                            else
                                            {

                                                everySimbol -= key;
                                            }
                                        }
                                    }


                                    simbol = Convert.ToChar(everySimbol);
                                }


                                afterString += simbol;

                            }
                            will.Add(afterString);

                        }
                    }
                }

            }
            catch (Exception exc)
            {
                will.Add(exc.Message);
            }
            return will;
        }
        public ArrayList TextFile(string filename)
        {
            string appDataPath = System.Web.HttpContext.Current.Server.MapPath(@"~/Files");
            string absolutePathToFile = Path.Combine(appDataPath, filename);
            ArrayList listOfValue = new ArrayList();
            if (absolutePathToFile.Contains(".txt")) //если файл расширения txt
            {
                StreamReader sr = new StreamReader(absolutePathToFile, Encoding.GetEncoding(1251));
                string line = sr.ReadLine();
                listOfValue.Add(line);

                while (line != null)
                {

                    line = sr.ReadLine();
                    listOfValue.Add(line); //считывание построчно и добавление в массив
                }

                sr.Close();
            }
            else if (absolutePathToFile.Contains(".docx"))
            {
                var docx = DocX.Load(absolutePathToFile);
                using (DocX document = DocX.Load(absolutePathToFile))
                {
                    listOfValue.Add(document.Text);
                }
            }
            return listOfValue;
        }
        public ArrayList AutoShifr(ArrayList listOfValue, string absolutePathToFile)
        {

            ArrayList library = new ArrayList();
            StreamReader sr = new StreamReader(absolutePathToFile);
            string line = sr.ReadLine();

            library.Add(line);

            while (line != null)
            {

                line = sr.ReadLine();
                library.Add(line); // добавление всех строчек из словаря в массив 
            }

            sr.Close();
            ArrayList textwill = new ArrayList();
            int i = 0; //первый ключ =0
            while (i < 33)
            {
                int countofsimular = 0;
                int countofrus = 0;
                textwill = Shifr(1, i, listOfValue); //шифрование исходного массива с ключем
                foreach (string s in textwill)
                {
                    string w = ""; //переменная для каждого слова

                    if (s != null)
                    {
                        char[] charstring = s.ToCharArray();
                        bool rus = true;
                        for (int charstr = 0; charstr < charstring.Length; charstr++)
                        {
                            if ((charstring[charstr] >= 1040) && (charstring[charstr] <= 1103) || (charstring[charstr] == 1105) || (charstring[charstr] == 1025))//если символ принадлежит к русскому алфавиту
                            {
                                w += charstring[charstr];
                                if (charstr == charstring.Length - 1)
                                {
                                    if (rus == true)
                                    {
                                        countofrus++;
                                        string str = w.ToLower();
                                        if (library.Contains(str))
                                        {
                                            countofsimular++;
                                        }
                                        w = "";
                                    }
                                }

                            }
                            else
                            {
                                if ((charstring[charstr] <= 32) && (charstring[charstr] <= 47) || (charstring[charstr] <= 58) && (charstring[charstr] <= 64)) //если знаки препинания либо пробел
                                {
                                    if ((rus == true) && (w != ""))
                                    {
                                        countofrus++;
                                        string str = w.ToLower();
                                        if (library.Contains(str))
                                        {
                                            countofsimular++; //если слово содержится в словаре 
                                        }
                                        w = "";
                                    }
                                }
                                else
                                {
                                    rus = false;
                                }

                            }

                        }

                    }
                }


                if (countofsimular > countofrus * 0.8)
                {
                    i = 36;
                }
                else
                {
                    if (i == 32)
                    {
                        textwill.Add("  Словарь не смог подобрать ключ");
                        i = 36;
                    }
                    else
                    {
                        i++;
                    }
                }
            }


            return textwill;
        }




    }
    class File
    {
        public string path;
        public ArrayList filevalue = new ArrayList();
        public List<string> will = new List<string>();

        public File(string path, ArrayList filevalue)
        {
            this.path = path;
            this.filevalue = filevalue;
        }
        public File()
        {

        }
    }

}