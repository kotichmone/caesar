﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SimanovichCaesarKurs.Models;
using System.Collections;
using System.Web.Routing;
using Xceed.Words.NET;
using System.IO;
using System.Text;

namespace SimanovichCaesarKurs.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            shifr text = new shifr();
            ViewBag.Textkey = 1; //изначальное значение ключа -1 
            return View();
        }
        [HttpPost]
        public ActionResult Save(HttpPostedFileBase load, string save, string textwill)
        {

            try
            {
                shifr text = new shifr();
                ArrayList list = new ArrayList();


                string fileName = System.IO.Path.GetFileNameWithoutExtension(load.FileName); //получаем желаемое пользователем имя файла
                if (save == "docx") //выполняется в случае, если указан тип docx
                {

                    string ext = AppDomain.CurrentDomain.BaseDirectory;
                    ext += "Files/" + fileName + ".docx";

                    var doc = DocX.Create(ext);
                    string testsave = textwill;
                    doc.InsertParagraph(testsave);

                    doc.Save(); //сохраняем файл в локальной папке к которой есть доступ с запрашиваемым именем и расширением
                    doc.Dispose();
                }
                else //выполняется в случае, если указан тип txt
                {
                    string ext = AppDomain.CurrentDomain.BaseDirectory;
                    ext += "Files/" + fileName + ".txt";

                    FileStream fstream = null;
                    fstream = new FileStream(ext, FileMode.CreateNew);
                    string testsave = textwill;

                    byte[] input = Encoding.Default.GetBytes(testsave); //преобразование расшифрованного текста в массив байтов 

                    fstream.Write(input, 0, input.Length);

                    fstream.Close();
                    fstream.Dispose();
                }

                ViewBag.Textexc = "Файл успешно сохранен"; //извещение пользователя о том, что файл с результатом сохранен


            }
            catch (Exception exc)
            {
                ViewBag.Textexc = exc.Message;
            }
            return View("Index");
        }
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload, string vector, string textkey, string adr, string textload, string auto)
        {
            string authData = $"{vector}"; //направление шифрования/дешифрования
            string key = $"{textkey}"; //значение ключа 
            shifr text = new shifr();
            ViewBag.Textkey = key;
            int keyvalue = 0;
            try
            {
                keyvalue = Convert.ToInt32(key); //конвертирование полученного значения в целочисленное 
            }
            catch (Exception exc)
            {
                ViewBag.Textexc = exc.Message;
            }
            ArrayList list = new ArrayList();
            string fileName = "";
            if (adr == "file")
            {
                try
                {
                    if (upload != null)
                    {

                        // получаем имя файла
                        fileName = System.IO.Path.GetFileName(upload.FileName);
                        // сохраняем файл в папку Files в проекте
                        upload.SaveAs(Server.MapPath("~/Files/" + fileName));
                        list = text.TextFile(fileName);
                    }
                }
                catch (Exception exc)
                {
                    ViewBag.Textexc = exc.Message;
                }
            }
            else if (adr == "text")
            {
                try { list.Add(textload); }
                catch (Exception exc)
                {
                    ViewBag.Textexc = exc.Message;
                }
            }


            ArrayList listwill = new ArrayList();

            int vectorvalue = 0;
            if (authData == "left")
            {
                vectorvalue = 1;
            }
            if (auto == "auto")
            {
                string appDataPath = System.Web.HttpContext.Current.Server.MapPath(@"~/Files");
                string absolutePathToFile = Path.Combine(appDataPath, "wordlib.txt");
                listwill = text.AutoShifr(list, absolutePathToFile); //метод вызывается в случае, если пользователь запросил автоподбор
            }
            else
            {
                listwill = text.Shifr(vectorvalue, keyvalue, list);
            }


            string s = "";
            foreach (string value in list)
            {
                s += value;
            }
            string swill = "";
            foreach (string value in listwill)
            {
                swill += value;
            }
            ViewBag.TextFile = s;
            ViewBag.TextFilewill = swill;

            return View("Index");
        }
    

            public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}