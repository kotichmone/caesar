﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimanovichCaesarKurs;
using SimanovichCaesarKurs.Controllers;
using System.Collections;
using SimanovichCaesarKurs.Models;

namespace SimanovichCaesarKurs.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
       
            [TestMethod]
            public void Index()
            {
                // Arrange
                HomeController controller = new HomeController();

                // Act
                ViewResult result = controller.Index() as ViewResult;

                // Assert
                Assert.IsNotNull(result);


                Assert.AreEqual("1", result.ViewBag.Textkey);
            }
            [TestMethod]
            public void BasicTest()
            {
                shifr text = new shifr();
                string expected = "Поздравляю, ты получил исходный текст!!!";
                ArrayList exp = new ArrayList();
                exp.Add(expected);
                string send = "Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!";
                ArrayList sending = new ArrayList();
                sending.Add(send);
                int key = 2;
                int vector = 1;
                sending = text.Shifr(vector, key, sending);
                string test = "";
                foreach (string s in sending)
                {
                    test += s;
                }
                Assert.AreEqual(expected, test);


            }
            [TestMethod]
            public void BasicTestauto()
            {
                shifr text = new shifr();
                string expected = "Поздравляю, ты получил исходный текст!!!В принципе понять, что тут используется шифр Цезаря не особо трудно,";
                ArrayList exp = new ArrayList();
                exp.Add(expected);
                string send = "Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!Д сткпшксж српбфю, щфр фхф кусрнюйхжфуб ъкцт Шжйвтб пж рургр фтхёпр,";
                ArrayList sending = new ArrayList();
                sending.Add(send);
            string ext = AppDomain.CurrentDomain.BaseDirectory; //в этом куске кода была ошибка в прошлый раз
            string path = ext+@"\wordlib.txt";
                sending = text.AutoShifr(sending, path);
                string test = "";
                foreach (string s in sending)
                {
                    test += s;
                }
                Assert.AreEqual(expected, test);


            }

            [TestMethod]
            public void About()
            {
                // Arrange
                HomeController controller = new HomeController();

                // Act
                ViewResult result = controller.About() as ViewResult;

                // Assert
                Assert.AreEqual("About", result.ViewName);
            }

            [TestMethod]
            public void Contact()
            {

                HomeController controller = new HomeController();

                ViewResult result = controller.Contact() as ViewResult;

                Assert.IsNotNull(result);
            }
        
    }
}
